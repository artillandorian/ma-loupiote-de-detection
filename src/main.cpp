#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>


#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

const int trigPin = 12;
const int echoPin = 14;

const int trigPin2 = 07;
const int echoPin2 = 06;

const int LEDROUGE = 0;
const int LEDVERT = 2;


//define sound velocity in cm/uS
#define SOUND_VELOCITY 0.034

long duration;
float distanceCm;
long duration2;
float distanceCm2;
float distanceInch;
int count_passage;
bool is_here;






void setup() {
  Serial.begin(9600); // Starts the serial communication

 
   pinMode(LEDROUGE, OUTPUT); // Sets the trigPin as an Output
   pinMode(LEDVERT, OUTPUT); // Sets the trigPin as an Output
 
   pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
   pinMode(echoPin, INPUT); // Sets the echoPin as an Input
   Serial.print("setup : avant");
   // pinMode(trigPin2, OUTPUT); // Sets the trigPin as an Output
   // pinMode(echoPin2, INPUT); // Sets the echoPin as an Input
   Serial.print("setup : apres");
   
   count_passage = 0;
   is_here = false;
 
   if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
     Serial.println(F("SSD1306 allocation failed"));
     for(;;);
   }
 
   delay(1500);
   display.clearDisplay();
 
   display.setTextSize(2);
   display.setTextColor(WHITE);


}


void loop() {
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  // digitalWrite(trigPin2, LOW);
  // delayMicroseconds(2);
  // digitalWrite(trigPin2, HIGH);
  // delayMicroseconds(10);
  // digitalWrite(trigPin2, LOW);

  
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  // duration2 = pulseIn(echoPin2, HIGH);
  
  // Calculate the distance
  distanceCm = duration * SOUND_VELOCITY/2;
  // distanceCm2 = duration2 * SOUND_VELOCITY/2;
  

  if(distanceCm<50){
    if(is_here == false){
      digitalWrite(LEDROUGE,HIGH);
      digitalWrite(LEDVERT,LOW);
      count_passage = count_passage + 1;
      Serial.print("Passage : ");
      Serial.println(count_passage);
      is_here = true;
      delay(750);
    }
  }
  else{
    is_here = false;
    digitalWrite(LEDVERT,HIGH);
    digitalWrite(LEDROUGE,LOW);
  }

  //  Serial.print(distanceCm2);
  // if(distanceCm2<50){
  //     count_passage = count_passage + 1;
  //     //is_here = true;
  //     //delay(750);
    
  // }

  display.clearDisplay();
  display.setCursor(0, 0);
  
  display.print(count_passage);



  if(count_passage > 5){
    display.print((String)"\n Climatisation");
  }
  
  display.display(); 
  delay(10);
}

void callback(char *topic, byte *payload, unsigned int length) {
 Serial.print("Message arrived in topic: ");
 Serial.println(topic);
 Serial.print("Message:");
 
 for (int i = 0; i<length; i++) {
  Serial.print((char) payload[i]);
 }
 
}


